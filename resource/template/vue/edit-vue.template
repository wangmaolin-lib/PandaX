<template>
  <div class="system-menu-container">
    <el-dialog v-model="isShowDialog" width="769px">
      <template #title>
        <div style="font-size: large" v-drag="['.system-menu-container .el-dialog', '.system-menu-container .el-dialog__header']">{{"{{"}}title{{"}}"}}</div>
      </template>
      <el-form
        :model="ruleForm"
        :rules="ruleRules"
        ref="ruleFormRef"
        label-width="80px"
      >
        <el-row :gutter="35">
          <el-col :span="24" >
            <el-form-item label="名称" prop="name">
              <el-input
                v-model="ruleForm.name"
                placeholder="请输入名称"
              />
            </el-form-item>
          </el-col>

          <el-col :span="24" >
            <el-form-item label="状态" prop="status">
              <el-radio-group v-model="ruleForm.status">
                <el-radio
                  v-for="dict in statusOptions"
                  :key="dict.dictValue"
                  :label="dict.dictValue"
                  >{{"{{"}} dict.dictLabel {{"}}"}}
                </el-radio>
              </el-radio-group>
            </el-form-item>
          </el-col>
        </el-row>
      </el-form>
      <template #footer>
        <span class="dialog-footer">
          <el-button @click="onCancel" size="small">取 消</el-button>
          <el-button type="primary" @click="onSubmit" size="small"
            >编 辑</el-button
          >
        </span>
      </template>
    </el-dialog>
  </div>
</template>

<script lang="ts">
import { reactive, toRefs, ref, unref, getCurrentInstance } from "vue";
import { update{{.ClassName}}, add{{.ClassName}} } from "/@/api/{{.PackageName}}/{{.BusinessName}}";
import { ElMessage } from "element-plus";

export default {
  name: "editMenu",
  props: {
    // 弹窗标题
    title: {
      type: String,
      default: () => "",
    },
  },
  setup() {
    const { proxy } = getCurrentInstance() as any;
    const ruleFormRef = ref<HTMLElement | null>(null);
    const state = reactive({
      // 是否显示弹出层
      isShowDialog: false,

      ruleForm: {
        {{.PkJsonField}}: 0, // ID
        name: "", // 名称
        // 更多参数需要遍历，等-等
        status: "0",  //状态
      },
      // 状态数据字典
      statusOptions: [],
      // 表单校验
      ruleRules: {

      },
    });
    // 打开弹窗
    const openDialog = (row: any) => {
      if (row.{{.PkJsonField}} && row.{{.PkJsonField}} != undefined && row.{{.PkJsonField}} != 0) {
        state.ruleForm = row;
        state.ruleForm.{{.PkJsonField}}=row.{{.PkJsonField}}; // ID
        // 更多参数
      } else {
        initForm();
      }
      state.isShowDialog = true;

      // 查询状态数据字典
      proxy.getDicts("sys_normal_disable").then((response: any) => {
        state.statusOptions = response.data;
      });
    };

    // 关闭弹窗
    const closeDialog = (row?: object) => {
      proxy.mittBus.emit("onEdit{{.ClassName}}Module", row);
      state.isShowDialog = false;
    };
    // 取消
    const onCancel = () => {
      closeDialog();
    };

    // 保存
    const onSubmit = () => {
      const formWrap = unref(ruleFormRef) as any;
      if (!formWrap) return;
      formWrap.validate((valid: boolean) => {
        if (valid) {
          if (
            state.ruleForm.{{.PkJsonField}} != undefined &&
            state.ruleForm.{{.PkJsonField}} != 0
          ) {
            update{{.ClassName}}(state.ruleForm).then((response) => {
              ElMessage.success("修改成功");
              closeDialog(state.ruleForm); // 关闭弹窗
            });
          } else {
            add{{.ClassName}}(state.ruleForm).then((response) => {
              ElMessage.success("新增成功");
              closeDialog(state.ruleForm); // 关闭弹窗
            });
          }
        }
      });
    };
    // 表单初始化，方法：`resetFields()` 无法使用
    const initForm = () => {
      state.ruleForm.{{.PkJsonField}} = 0; // ID
      // 更多参数初始化
    };

    return {
      ruleFormRef,
      openDialog,
      closeDialog,
      onCancel,
      initForm,
      onSubmit,
      ...toRefs(state),
    };
  },
};
</script>
